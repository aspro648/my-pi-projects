#http://learn.adafruit.com/debugging-with-the-raspberry-pi-webide/debug-a-blinking-led

import RPi.GPIO as GPIO
from time import sleep

LED_PINS = [17, 21, 22, 23]
blip = 0.25

import socket
import fcntl
import struct
import sys

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])

def blink(num):
	LED_PIN = 17 #23
	if num > 0:
		for x in range(num):
			sleep(blip)
			GPIO.output(LED_PIN, True)
			sleep(blip)
			GPIO.output(LED_PIN, False)	
	else:
		GPIO.output(LED_PIN, True)
		sleep(blip * 8)
		GPIO.output(LED_PIN, False)
		

def blinkIP(ip):
	digits = ip.split('.')
	print digits
	for piece in digits:
		for digit in piece:
			print "blink ", digit
			blink(int(digit))
			sleep(blip * 4)
		print "dot"
		sleep(blip * 8)



print "Setting up GPIO"
GPIO.setmode(GPIO.BCM)
for pin in LED_PINS:
	GPIO.setup(pin, GPIO.OUT)
	GPIO.output(pin, False)

def enable_led(should_enable):
	if should_enable:
		GPIO.output(LED_PIN, True)
	else:
		GPIO.output(LED_PIN, False)

	
nws = ['eth0', 'wlan0', 'wlan1']
ip = ''

for nw in nws:
	try:
		ip = get_ip_address(nw)
	except IOError:
		print "NO ", nw

if ip != '':
	for x in range(2):
		blinkIP(ip)
		sleep(4)
		
GPIO.cleanup()