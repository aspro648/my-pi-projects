#http://learn.adafruit.com/debugging-with-the-raspberry-pi-webide/debug-a-blinking-led

import RPi.GPIO as GPIO
from time import sleep

LED_PINS = [17, 21, 22]

import socket
import fcntl
import struct
import sys

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])

def blink(num):
	LED_PIN = 17
	if num > 0:
		for x in range(num):
			sleep(0.5)
			GPIO.output(LED_PIN, True)
			sleep(0.5)
			GPIO.output(LED_PIN, False)	
	else:
		GPIO.output(LED_PIN, True)
		sleep(2)
		GPIO.output(LED_PIN, False)
		

def blinkIP(ip):
	digits = ip.split('.')
	print digits
	for piece in digits:
		for digit in piece:
			print "blink ", digit
			blink(int(digit))
			sleep(1)
		print "dot"
		sleep(2)



print "Setting up GPIO"
GPIO.setmode(GPIO.BCM)
for pin in LED_PINS:
	GPIO.setup(pin, GPIO.OUT)
	GPIO.output(pin, False)

def enable_led(should_enable):
	if should_enable:
		GPIO.output(LED_PIN, True)
	else:
		GPIO.output(LED_PIN, False)

		
ip = get_ip_address('wlan0')
while True:
	blinkIP(ip)
	sleep(4)
sys.exit()

while True:
	for pin in LED_PINS:
		GPIO.output(pin, True)
		sleep(2)
		GPIO.output(pin, False)
		
enable_led(False)
print "LED is OFF"
sleep(2)
enable_led(True)
print "LED is ON"
sleep(2)
enable_led(False)
print "LED is OFF"
sleep(2)
enable_led(True)
print "LED is ON"
sleep(2)
GPIO.cleanup()