# coding: utf-8

# web.py: controlling your Pi from a smartphone
# using LedBorg (www.piborg.com) as an example
# ColinD 27/12/2012


import RPi.GPIO as GPIO
import sys
import web
from web import form

# setup GPIO stuff
green = 17
yellow = 18
red = 21

colors = [ green, yellow, red ]

GPIO.setmode(GPIO.BCM)
for color in colors:
	GPIO.setup(color, GPIO.OUT)
	GPIO.output(color, False)

# Define the pages (index) for the site
urls = ('/', 'index')
render = web.template.render('templates')
app = web.application(urls, globals())

# Define the buttons that should be shown
my_form = form.Form(
    form.Button("btn", id="btnG", value="G", html="Green", class_="btnGreen"),    
    form.Button("btn", id="btnY", value="Y", html="Yellow", class_="btnYellow"),
    form.Button("btn", id="btnR", value="R", html="Red", class_="btnRed"),    
    form.Button("btn", id="btnO", value="0", html="-off-", class_="btnOff")
)


# define what happens when the index page is called on the form
class index:
    # GET us used when the page is first requested
    def GET(self):
        form = my_form()
        return render.index(form, "Raspberry Pi Python Remote Control")
    
    # POST is called when a web form is submitted
    def POST(self):
        # get the data submitted from the web form
        userData = web.input()
        # Determine which colour LedBorg should display
        if userData.btn == "Y":
            print "YELLOW"
            GPIO.output(yellow, True)
            GPIO.output(green, False)
            GPIO.output(red, False)
        elif userData.btn == "G":
            print "GREEN"
            GPIO.output(yellow, False)
            GPIO.output(green, True)
            GPIO.output(red, False)
        elif userData.btn == "R":
            print "RED"
            GPIO.output(yellow, False)
            GPIO.output(green, False)
            GPIO.output(red, True)            
        elif userData.btn == "0":
            print "Off"
            GPIO.output(yellow, False)
            GPIO.output(green, False)
            GPIO.output(red, False)  
        else:
            print "Do nothing else ­ assume something fishy is going on..."
            
    
        # reload the web form ready for the next user input
        raise web.seeother('/')
        
# run
if __name__ == '__main__':
    app.run()
