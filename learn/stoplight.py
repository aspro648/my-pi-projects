import RPi.GPIO as GPIO
import time

ON = True
OFF = False
GREEN = 11
YELLOW = 12
RED = 13
COLORS = [GREEN, YELLOW, RED]

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

for COLOR in COLORS:
	GPIO.setup(COLOR, GPIO.OUT)

def LED(COLOR, val):
	if val:
		GPIO.output(COLOR, GPIO.HIGH)
	else:
		GPIO.output(COLOR, GPIO.LOW)


if __name__ == "__main__":
	
	for i in range(5):
		for COLOR in COLORS:
			LED(COLOR, ON)
			time.sleep(1)
			LED(COLOR, OFF)
	GPIO.cleanup()

	