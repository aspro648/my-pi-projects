# coding: utf-8

# web.py: controlling your Pi from a smartphone
# using LedBorg (www.piborg.com) as an example
# ColinD 27/12/2012


import RPi.GPIO as GPIO
from time import sleep
import sys
import web
from web import form

# setup GPIO stuff
led_pins = [17, 21, 22]
GPIO.setmode(GPIO.BCM)
for pin in led_pins:
	GPIO.setup(pin, GPIO.OUT)
	GPIO.output(pin, False)

# Define the pages (index) for the site
urls = ('/', 'index')
render = web.template.render('templates')
app = web.application(urls, globals())

# Define the buttons that should be shown
my_form = form.Form(
    form.Button("btn", id="btnG", value="0", html="Green ", class_="btnGreen"),    
    form.Button("btn", id="btnY", value="1", html="Yellow", class_="btnYellow"),
    form.Button("btn", id="btnR", value="2", html="Red   ", class_="btnRed"),    
    #form.Button("btn", id="btnO", value="OFF", html="-off-", class_="btnOff")
)


# define what happens when the index page is called on the form
class index:
    # GET us used when the page is first requested
    def GET(self):
        form = my_form()
        return render.index(form, "Raspberry Pi Python Remote Control")
    
    # POST is called when a web form is submitted
    def POST(self):
        # get the data submitted from the web form
        userData = web.input()
        # Determine which colour LedBorg should display
        try:
        	val = led_pins[int(userData.btn)]
        except:
        	val = userData.btn
		print val
		
        for pin in led_pins:
        	GPIO.output(pin, False)
        	if val in led_pins:
        		GPIO.output(val, True)
        		
		if val == "OFF":
			while True:
				for pin in led_pins:
					GPIO.output(pin, True)
					sleep(2)
					GPIO.output(pin, False)


        # reload the web form ready for the next user input
        raise web.seeother('/')
        
# run
if __name__ == '__main__':
    app.run()
