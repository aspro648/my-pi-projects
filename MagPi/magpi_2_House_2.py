# THE HOUSE

# From MagPi Issue 2 (http://www.themagpi.com/)
# Requires pyGame (http://www.pygame.org/)
# Modified to show what is going on.


import os, pygame
from pygame.locals import *


def build(message, commands):
    ''' Executes pygame commands from a list and updates screen. '''
    print '\n', message
    for command in commands:
        print '\t%s' % command
        exec(command)
        pygame.display.set_caption(message)
        pygame.display.update()
        pygame.time.wait(500)
    pygame.time.wait(1000)

def pygame_screen():
    "Ininitializes a new pygame screen using the framebuffer"
    # Based on "Python GUI in Linux frame buffer"
    # http://www.karoltomala.com/blog/?p=679

    disp_no = os.getenv("DISPLAY")
    if disp_no:
        print "I'm running under X display = {0}".format(disp_no)
    
    # Check which frame buffer drivers are available
    # Start with fbcon since directfb hangs with composite output
    drivers = ['fbcon', 'directfb', 'svgalib']
    found = False
    for driver in drivers:
        # Make sure that SDL_VIDEODRIVER is set
        if not os.getenv('SDL_VIDEODRIVER'):
            os.putenv('SDL_VIDEODRIVER', driver)
        try:
            pygame.display.init()
        except pygame.error:
            print 'Driver: {0} failed.'.format(driver)
            continue
        found = True
        break

    if not found:
        raise Exception('No suitable video driver found!')
    
    size = (pygame.display.Info().current_w, pygame.display.Info().current_h)
    print "Framebuffer size: %d x %d" % (size[0], size[1])
    screen = pygame.display.set_mode(size, pygame.FULLSCREEN)
    # Clear the screen to start
    screen.fill((0, 0, 0))        
    # Initialise font support
    pygame.font.init()
    # Render the screen
    pygame.display.update()
    return screen

# Initialize pyGame and screen
#pygame.init()
clock = pygame.time.Clock()
#os.environ['SDL_VIDEO_WINDOW_POS'] = 'center'
d = pygame.draw # this will save us writing 'pygame.draw' many times
#screen = pygame.display.set_mode([423,347],0,32)
screen = pygame_screen()
#screen.fill((186, 213, 48)) #background screen colour
#pygame.display.update()
  
# Define some colors
white = (255,255,255)
black = (0,0,0)
walls = (157, 109, 9)
door_dark = (151, 36, 9)
door_light =(181, 132, 14)

# Build wall
wall = ['d.rect(screen, black, (60, 102, 305, 225) )',
        'd.rect(screen, walls, (73, 114, 280, 200) )']
build('Building Wall', wall)

# and the roof
roof = ['d.polygon(screen,black,((35,112),(121,12),(296,12),(321, 36), \
                        (321, 12),(361, 12),(361, 84),(384, 112)))',
        'd.polygon(screen,walls,((62,101),(128,23),(289,23),(334, 69), \
                        (334, 25),(348, 25),(348, 88),(361, 101)))']
build('The Roof', roof)

# and the door
door = ['d.rect(screen, black, (167, 198, 84, 125 ))',
        'd.rect(screen, door_dark, (179, 210, 60, 101 ))',
        'd.rect(screen, black, (185, 216, 50, 54 ))',
        'd.rect(screen, door_light, (191, 222, 38, 41 ))',
        'd.circle(screen, black,(192, 277),5)' ]
build('The Door', door)

# and the windows
window_coords = [(82, 125), (82, 215), (262, 125), (262, 215)]
for window in window_coords:
    cmds = ['d.rect(screen, black, (window[0], window[1], 76, 76) )',
            'd.rect(screen, white, (window[0] + 12, window[1] + 12, 22, 22) )',
            'd.rect(screen, white, (window[0] + 42, window[1] + 12, 22, 22) )',
            'd.rect(screen, white, (window[0] + 12, window[1] + 42, 22, 22) )',
            'd.rect(screen, white, (window[0] + 42, window[1] + 42, 22, 22) )' ]
    build('A window at ' + str(window), cmds)
    
# Finish up
build('My House', [''])
pygame.image.save(screen, 'My_House.png')
print 'Saved "My_House.png" to \n%s' % os.getcwd()
print '\nClosing pyGame window in 10 seconds . . .'
pygame.time.wait(10000)
pygame.quit()
