# Test script to show how webIDE Visualize works

def add(a, b):
	c = a + b
	return c
	
a = 1
b = {'name':'Ken'}
a += 1
b['last'] = 'Olsen'
c = (21, 56)
d = add(22, 34)

x = [1, 2, 3]
y = [4, 5, 6]
z =y
y =x
x =z

x = [1, 2, 3]
y =x
x.append(4)
y.append(5)
z = [1, 2,3 ,4, 5]
x.append(6)
y.append(7)
y = "hello"