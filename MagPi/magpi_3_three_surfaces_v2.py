# THREE SURFACES V2
# By Jaseman - 13 June 2012
import sys, os, pygame; from pygame.locals import *

def pygame_screen():
    "Ininitializes a new pygame screen using the framebuffer"
    # Based on "Python GUI in Linux frame buffer"
    # http://www.karoltomala.com/blog/?p=679

    disp_no = os.getenv("DISPLAY")
    if disp_no:
        print "I'm running under X display = {0}".format(disp_no)
    
    # Check which frame buffer drivers are available
    # Start with fbcon since directfb hangs with composite output
    drivers = ['fbcon', 'directfb', 'svgalib']
    found = False
    for driver in drivers:
        # Make sure that SDL_VIDEODRIVER is set
        if not os.getenv('SDL_VIDEODRIVER'):
            os.putenv('SDL_VIDEODRIVER', driver)
        try:
            pygame.display.init()
        except pygame.error:
            print 'Driver: {0} failed.'.format(driver)
            continue
        found = True
        break

    if not found:
        raise Exception('No suitable video driver found!')
    
    size = (pygame.display.Info().current_w, pygame.display.Info().current_h)
    print "Framebuffer size: %d x %d" % (size[0], size[1])
    screen = pygame.display.set_mode(size, pygame.FULLSCREEN)
    # Clear the screen to start
    screen.fill((0, 0, 0))        
    # Initialise font support
    pygame.font.init()
    # Render the screen
    pygame.display.update()
    return screen

#pygame.init()
clock = pygame.time.Clock()
#os.environ['SDL_VIDEO_WINDOW_POS'] = 'center'
#pygame.display.set_caption("Three Surfaces")
#screen = pygame.display.set_mode([400,200],0,32) # The main screen
screen = pygame_screen()
sky = pygame.Surface((400,200)) # A sky surface
sky.fill((200,255,255)) # Fill the surface in light blue color
grass = pygame.Surface((400,100)) # A grass surface
grass.fill((50,150,50)) # Fill the surface in green color
sun = pygame.Surface((40,40)) # A sun surface
sun.set_colorkey([0,0,0])
pygame.draw.circle(sun,(255,255,0),(20,20),20)
while True: # A never ending loop to keep the program running
	for event in pygame.event.get():
		if event.type == QUIT:
			pygame.quit()
			sys.exit()
		mousex,mousey = pygame.mouse.get_pos()
		screen.blit(sky,(0,0)) # Paste the sky surface at x,y
		screen.blit(sun,(mousex,mousey)) # Paste the sun surface at x,y
		screen.blit(grass,(0,100)) # Paste the grass surface at x,y
		pygame.display.update()
