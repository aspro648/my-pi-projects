#!/usr/bin/python

import time
import datetime
from Adafruit_LEDBackpack import LEDBackpack

# ===========================================================================
# 8x8 Pixel Display
# ===========================================================================

class EightByEight:
  disp = None

  # Constructor
  def __init__(self, address=0x70, debug=False):
    if (debug):
      print "Initializing a new instance of LEDBackpack at 0x%02X" % address
    self.disp = LEDBackpack(address=address, debug=debug)


  def writeRowRaw(self, charNumber, value):
    "Sets a row of pixels using a raw 16-bit value"
    if (charNumber > 7):
      return
    # Set the appropriate row
    self.disp.setBufferRow(charNumber, value)

  def clearPixel(self, x, y):
    "A wrapper function to clear pixels (purely cosmetic)"
    self.setPixel(x, y, 0)

  def setPixel(self, x, y, color=1):
    "Sets a single pixel"
    if (x >= 8):
      return
    if (y >= 8):
      return
    x += 7
    x %= 8
    # Set the appropriate pixel
    buffer = self.disp.getBuffer()
    if (color):
      self.disp.setBufferRow(y, buffer[y] | 1 << x)
    else:
      self.disp.setBufferRow(y, buffer[y] & ~(1 << x))

  def clear(self):
    "Clears the entire display"
    self.disp.clear()

# https://github.com/reddakota/Adafruit-Raspberry-Pi-Python-Code/blob/master/Adafruit_LEDBackpack/Adafruit_8x8.py
# added by user reddakota 2012-12-01
# color: 0=clear 1=green 2=red 3=orange
  def setBicolorPixel(self, x, y, color=1):
    if(x >= 8):
	  return	
    if(y >= 8):
	  return

    buffer = self.disp.getBuffer()

    rowBuffer = buffer[y]

    if color == 0:
	  rowBuffer &= ~(1 << x)
	  x += 8
	  rowBuffer &= ~(1 << x)
    elif color == 1:
	  rowBuffer &= ~(1 << x)
	  x += 8
	  rowBuffer &= ~(1 << x)
	  x -= 8
	  rowBuffer |= 1 << x
    elif color == 2:   	
	  rowBuffer &= ~(1 << x)
	  x += 8
	  rowBuffer |= 1 << x
    elif color == 3:
	  rowBuffer |= 1 << x
	  x += 8
	  rowBuffer |= 1 << x
	    
    self.disp.setBufferRow(y, rowBuffer)	
 
	