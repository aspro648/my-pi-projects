#!/usr/bin/python

import copy
import time
import datetime
from Adafruit_8x8 import EightByEight
from Adafruit_LEDBackpack import LEDBackpack
import random


# ===========================================================================
# 8x8 Pixel Example
# ===========================================================================
#grid = EightByEight(address=0x70)

print "Press CTRL+Z to exit"

led = LEDBackpack(0x70)
led.setBrightness(5)
led.setBlinkRate(0)
led = EightByEight(address=0x70)
# Continually update the 8x8 display one pixel at a time

blank = [[0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0]]

grid = copy.deepcopy(blank)
glide = [[0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
		[0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0]]
        
glide = [[0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 0, 0, 0, 0],
		[0, 0, 1, 0, 0, 0, 0, 0],
		[0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0]]
        
age = copy.deepcopy(grid)
grid_next = copy.deepcopy(blank)
grid_prev = copy.deepcopy(blank)
neighbors = copy.deepcopy(blank)
population = 1

def printGrid(grid):
    for x in range(7, -1, -1):
        for y in range(8):
            print grid[x][y],
        print
    print


def displayGrid(grid):
	for x in range(8):
		for y in range(8):
			led.setBicolorPixel(x, y, color=grid[x][y])


def loadGrid():
    global grid
    global population
    # load initial grid
    population = 0
    sides = 2
    for row in range(8):
        for col in range(8):
            roll = int(random.random() * sides)
            if roll: 
                grid[row][col] = 1
                population += 1
            else:
                grid[row][col] = 0


def countNeighbors():
    for row in range(8):    
        for col in range(8):
            count = 0
            for i in range(-1, 2):
                for j in range(-1, 2):
                    if row + i > 7: i = -7
                    if col + j > 7: j = -7
                    if i == 0 and j == 0:
                        pass
                    else:
                        if grid[row + i][col + j] > 0:
                            count += 1
            neighbors[row][col] = count         


def checkGrid():
    global grid
    global population
    global grid_prev
    population = 0
    for row in range(8):
        for col in range(8):
            if age[row][col] > 0: # cell is alive
                if neighbors[row][col] < 2: # dies lonely
                    grid_next[row][col] = 0
                    age[row][col] = 0
                elif neighbors[row][col] < 4: #lives
                    grid_next[row][col] = 1
                    age[row][col] += 1
                    population += 1
                    if age[row][col] > 3: age[row][col] = 3
                else: # dies crowded
                    age[row][col] = 0
                    grid_next[row][col] = 0
            else:
                if neighbors[row][col] == 3: #born
                    grid_next[row][col] = 1
                    age[row][col] = 1
                    population += 1
    displayGrid(age)
    time.sleep(1)
    if grid == grid_next or grid == grid_prev:
		population = 0
    else:
		grid_prev = copy.deepcopy(grid)
		grid = copy.deepcopy(grid_next)

while True:
    loadGrid()
    countNeighbors()
    while population:
        #print 'pop = ', population
        #printGrid(grid)
        countNeighbors()        
        #printGrid(neighbors)
        #raw_input("press key to continue")
        checkGrid()
