Help on class EightByEight in module Adafruit_8x8:

class EEiigghhttBByyEEiigghhtt
 |  Methods defined here:
 |  
 |  ____iinniitt____(self, address=112, debug=False)
 |      # Constructor
 |  
 |  cclleeaarr(self)
 |      Clears the entire display
 |  
 |  cclleeaarrPPiixxeell(self, x, y)
 |      A wrapper function to clear pixels (purely cosmetic)
 |  
 |  sseettPPiixxeell(self, x, y, color=1)
 |      Sets a single pixel
 |  
 |  wwrriitteeRRoowwRRaaww(self, charNumber, value)
 |      Sets a row of pixels using a raw 16-bit value
 |  
 |  ----------------------------------------------------------------------
 |  Data and other attributes defined here:
 |  
 |  ddiisspp = None
