# http://talk.maemo.org/showthread.php?t=60178

import cwiid
import time

print 'Press 1+2 on your Wiimote now...'
wm = cwiid.Wiimote()

time.sleep(1)

wm.rpt_mode = cwiid.RPT_BTN | cwiid.RPT_ACC
i = 0


while True:
    wm.led = i
    i += 1
    if i ==16: i = 0
    wm.rumble = (wm.state['acc'][0] < 126)
    if wm.state['buttons'] & cwiid.BTN_A:
        wm.led = (wm.state['led'] + 1) % 16
    print wm.state['acc']
    time.sleep(.5)