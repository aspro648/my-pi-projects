# http://www.cl.cam.ac.uk/~db434/raspi/wiimote/
from time import sleep
import cwiid
import scope
import time 
#import i2c 
#connecting to the Wiimote. This allows several attempts 
# as first few often fail. 

def flashLEDs(repeats, delay=0.1, exitVal=1, rumble=False):
	vals = [0, 1, 2, 4, 8]
	wm.rumble = rumble
	for repeat in range(repeats):
		for val in vals:
			wm.led = val
			sleep(delay)
	wm.led = exitVal
	wm.rumble = False

def checkButtons(buttons):
	rt = []
	for key, value in btn_dict.iteritems():
		if value | buttons == value:
			rt.append(key)
	return rt

colors = ((0, 255, 255), (255, 255, 0), (255,0 ,0 )) #cyan, yellow, red
btn_dict = {'BTN_1': 2, 'BTN_2': 1, 'BTN_A': 8, 'BTN_B': 4, 
            'BTN_DOWN': 1024, 'BTN_HOME': 128, 'BTN_LEFT': 256, 
            'BTN_MINUS': 16, 'BTN_PLUS': 4096, 'BTN_RIGHT': 512,
            'BTN_UP': 2048}

print 'Press 1+2 on your Wiimote to place in discovery mode ...' 
wm = None 
i = 0 
while not wm: 
    try: 
        wm = cwiid.Wiimote() 
    except RuntimeError: 
        if i > 2: 
            quit() 
            break 
            print "Error opening wiimote connection" 
        print "attempt " + str(i) 
        i += 1 
    sleep(1)

wm.rpt_mode = cwiid.RPT_BTN | cwiid.RPT_ACC
# flash lights on sucsess
print 'Connected!'
flashLEDs(1, exitVal=1, rumble=False)

#for key, value in wm.state.iteritems():
#	print key, ' = ', value
	
print wm.state
# {'acc': (130, 129, 155), 'led': 1, 'rpt_mode': 6, 'ext_type': 0, 'buttons': 0, '
#  rumble': 0, 'error': 0, 'battery': 93}    

initVals = [wm.state['acc'][0], wm.state['acc'][1], wm.state['acc'][2]]    


if __name__ == '__main__':

    myscope = scope.pyscope()
    myscope.draw_screen()
    myscope.drawGraticule()
   
    points = [[], [], []]
    for pointer in range(500):
    	for p in range(3):
    		points[p].append(0)
    pointer = 0    
    lastButtons = []
    
    while True:
        # read the analog pin
        for p in range(3):
        	#print pointer, p
        	points[p][pointer] = 250 + 250 * p + (wm.state['acc'][p] - initVals[p]) * 5
        	
        #point = 200 + (wm.state['acc'][0] - initX) * 5
		#print wm.state['acc']
		buttons = wm.state['buttons']
		if buttons > 0: # and buttons != lastButtons:
			if buttons != lastButtons:
				pressed = checkButtons(buttons)
				for press in pressed:
					print press
				lastButtons = buttons
				sleep(0.1)
		else:
			lastButtons = []
		
        if pointer > 0:
            # 1024 (max adc) / 320 (max y) = 3.2
            for p in range(3):
                pts = (pointer - 1, points[p][pointer - 1] / 3.2, pointer, points[p][pointer] / 3.2)
                myscope.draw_line(pts, color=colors[p])
            time.sleep(0.01)
        pointer += 1
        if pointer >= 500:
            pointer = 0
            myscope.drawGraticule()
   