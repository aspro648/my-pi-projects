
import datetime
import math
import pygame
import sys
import time

class timer:
    def __init__(self):
        self.start_time = time.time()

    def getRemainingTime(self):

        return time.time()-self.start_time


class visualClock:

    def __init__(self):
        print 'init'

        self.frame = 1
        #colour tupples based on RGB 255 values
        self.red = (255,0,0)
        self.blue = (0,0,255)
        self.green = (0,255,0)
        self.yellow = (255,255,0)
        self.magenta = (255,0,255)
        self.black = (0,0,0)
        self.white = (255,255,255)
        self.orange = (198,138,22)

        self.screen = pygame.display.set_mode((600, 600))
        pygame.init()
        self.font30 = pygame.font.Font(None, 30)
        self.font60 = pygame.font.Font(None, 60)
        

    def addNumbers(self):

        fontimg = self.font30.render('6 am',1,self.black)
        self.screen.blit(fontimg, (520,290))
        fontimg = self.font30.render('12 noon',1,self.black)
        self.screen.blit(fontimg, (285,520))
        fontimg = self.font30.render('6 pm',1,self.black)
        self.screen.blit(fontimg, (25,290))
        fontimg = self.font30.render('12 midnight',1,self.black)
        self.screen.blit(fontimg, (280,75))

    def run(self):
        print 'run'
        running = True
        while running:
            event = pygame.event.poll()
            if event.type == pygame.QUIT:
                running = False
                
            self.screen.fill(self.white)
            pygame.draw.circle(self.screen, self.blue, (300, 300), 200)
            self.addNumbers()

            #get time
            dt=str(datetime.datetime.today())
            #time = dt[11:19]
            fontimg = self.font60.render(dt[11:19],1,self.black)
            self.screen.blit(fontimg, (220,550))

            hr = float(dt[11:13])
            min = float(dt[14:16])
            sec = float(dt[17:19])
            hours_past_midnight = hr + min/60.0
            #print 'hours_past_midnight',hours_past_midnight
            degrees_past_midnight = hours_past_midnight*360.0/24.0
            for angle_degrees in range(int(degrees_past_midnight*10.0)):
                angle_radians = (angle_degrees*3.14/180.0)/10.0
                pygame.draw.aaline(self.screen, self.red, (300, 300), (300+200*math.sin(angle_radians), 300-200*math.cos(angle_radians)))

            if hours_past_midnight >= 7.00 and hours_past_midnight <= 7.50:
                pygame.draw.rect(self.screen, (100,100,100), (400,400, 550, 550))
                fontimg = self.font30.render('Breakfast',1,self.black)
                self.screen.blit(fontimg, (410,410))
                pygame.draw.circle(self.screen, self.yellow, (500, 500), 75)
            
            if hours_past_midnight >= 12.0 and hours_past_midnight <= 12.5:
                pygame.draw.rect(self.screen, (100,100,100), (0,400, 150, 550))
                fontimg = self.font30.render('Lunch',1,self.black)
                self.screen.blit(fontimg, (10,410))
            for loc in range(24):
                angle_radians = 2.0*3.1415*loc/24.0
                pygame.draw.circle(self.screen, self.black,
                                   (int(300+200*math.sin(angle_radians)),
                                    int(300-200*math.cos(angle_radians))), 8)
        
            pygame.display.flip()
            #self.frame += 1
            #print self.frame
            pygame.time.wait(50)

        pygame.quit()
        
        
if __name__ == '__main__':

    vc = visualClock()
    vc.run()
    #sys.exit(0)
