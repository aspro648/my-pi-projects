#!/usr/bin/env python

# http://learn.adafruit.com/reading-a-analog-in-and-controlling-audio-volume-with-the-raspberry-pi/script
# http://learn.adafruit.com/photocells/using-a-photocell

import time
import os, sys
import RPi.GPIO as GPIO
import scope

LEDpin = 23
LEDthreshold = 525 # when to turn LED on
buttonPin = 4
GPIO.setmode(GPIO.BCM)
GPIO.setup(LEDpin, GPIO.OUT) # for LED
GPIO.output(LEDpin, False)
GPIO.setup(buttonPin, GPIO.IN)

DEBUG = True

# read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
def readadc(adcnum, clockpin, mosipin, misopin, cspin):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
        GPIO.output(cspin, True)

        GPIO.output(clockpin, False) # start clock low
        GPIO.output(cspin, False) # bring CS low

        commandout = adcnum
        commandout |= 0x18 # start bit + single-ended bit
        commandout <<= 3 # we only need to send 5 bits here
        for i in range(5):
                if (commandout & 0x80):
                        GPIO.output(mosipin, True)
                else:
                        GPIO.output(mosipin, False)
                commandout <<= 1
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)

        adcout = 0
        # read in one empty bit, one null bit and 10 ADC bits
        for i in range(12):
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)
                adcout <<= 1
                if (GPIO.input(misopin)):
                        adcout |= 0x1

        GPIO.output(cspin, True)
        
        adcout >>= 1 # first bit is 'null' so drop it
        return adcout

# change these as desired - they're the pins connected from the
# SPI port on the ADC to the Cobbler
SPICLK = 17#18
SPIMISO = 21# 23
SPIMOSI = 22#24
SPICS = 24#25

# set up the SPI interface pins
GPIO.setup(SPIMOSI, GPIO.OUT)
GPIO.setup(SPIMISO, GPIO.IN)
GPIO.setup(SPICLK, GPIO.OUT)
GPIO.setup(SPICS, GPIO.OUT)


if __name__ == '__main__':
    adc_pin = 0 # ldr connected to MCP3008 analog pin 0
    myscope = scope.pyscope()
    myscope.draw_screen()
    myscope.drawGraticule()

    pointer = 0
    points = []
    while True:
        # read the analog pin
        point = 1023 - readadc(adc_pin, SPICLK, SPIMOSI, SPIMISO, SPICS)
        if len(points) < 500:
            points.append(point)
        else:
            points[pointer] = point
        if pointer > 0:
        	# 1024 (max adc) / 320 (max y) = 3.2
            pts = (pointer - 1, points[pointer - 1] / 3.2, pointer, points[pointer] / 3.2)
            myscope.draw_line(pts)
            time.sleep(0.01)
        pointer += 1
        if pointer >= 500:
            pointer = 0
            myscope.drawGraticule()
        if DEBUG: print point
        if point > LEDthreshold:
        	GPIO.output(LEDpin, True)
        else:
        	GPIO.output(LEDpin, False)
		
		if ( GPIO.input(buttonPin) == True ):
			GPIO.output(LEDpin, True)
			time.sleep(1)
			GPIO.output(LEDpin, False)