#!/usr/bin/env python

# http://learn.adafruit.com/reading-a-analog-in-and-controlling-audio-volume-with-the-raspberry-pi/script
# http://learn.adafruit.com/photocells/using-a-photocell

import time
import os, sys
import RPi.GPIO as GPIO
import scope
import random
sys.path.append('/usr/share/adafruit/webide/repositories/my-pi-projects/Adafruit_LEDBackpack')
from Adafruit_LEDBackpack import LEDBackpack
from Adafruit_8x8 import EightByEight

# setup LED matrix
grid = EightByEight(address=0x70)
led = LEDBackpack(0x70)
led.setBrightness(5)
led.setBlinkRate(0)

LEDpin = 23
LEDthreshold = 525 # when to turn LED on
buttonPin = 4
GPIO.setmode(GPIO.BCM)
GPIO.setup(LEDpin, GPIO.OUT) # for LED
GPIO.output(LEDpin, False)
GPIO.setup(buttonPin, GPIO.IN)

DEBUG = True

# read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
def readadc(adcnum, clockpin, mosipin, misopin, cspin):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
        GPIO.output(cspin, True)

        GPIO.output(clockpin, False) # start clock low
        GPIO.output(cspin, False) # bring CS low

        commandout = adcnum
        commandout |= 0x18 # start bit + single-ended bit
        commandout <<= 3 # we only need to send 5 bits here
        for i in range(5):
                if (commandout & 0x80):
                        GPIO.output(mosipin, True)
                else:
                        GPIO.output(mosipin, False)
                commandout <<= 1
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)

        adcout = 0
        # read in one empty bit, one null bit and 10 ADC bits
        for i in range(12):
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)
                adcout <<= 1
                if (GPIO.input(misopin)):
                        adcout |= 0x1

        GPIO.output(cspin, True)
        
        adcout >>= 1 # first bit is 'null' so drop it
        return adcout

# change these as desired - they're the pins connected from the
# SPI port on the ADC to the Cobbler
SPICLK = 17#18
SPIMISO = 21# 23
SPIMOSI = 22#24
SPICS = 24
SPICS2 = 25#24 or 25

# set up the SPI interface pins
GPIO.setup(SPIMOSI, GPIO.OUT)
GPIO.setup(SPIMISO, GPIO.IN)
GPIO.setup(SPICLK, GPIO.OUT)
GPIO.setup(SPICS, GPIO.OUT)
GPIO.setup(SPICS2, GPIO.OUT)
GPIO.output(SPICS2, True)


if __name__ == '__main__':
    #adc_pin = 0 # ldr connected to MCP3008 analog pin 0
    #myscope = scope.pyscope()
    #myscope.draw_screen()
    #myscope.drawGraticule()
    num_points = 9
    pointer = 0
    points = []
    
    while True:
        vals = []
        for adc_pin in range(num_points):
            if adc_pin < 8:
                #point = 1023 - readadc(adc_pin, SPICLK, SPIMOSI, SPIMISO, SPICS)
                point = readadc(adc_pin, SPICLK, SPIMOSI, SPIMISO, SPICS)
            else:
                #point = 1023 - readadc(adc_pin - 8, SPICLK, SPIMOSI, SPIMISO, SPICS2)
                point = readadc(adc_pin - 8, SPICLK, SPIMOSI, SPIMISO, SPICS2)
            vals.append(int(point / 1023.0 * 8))
            #print '\t%s' % int(point / 1023.0 * 7),
            #print '%s\t' % point,
        #print
        print vals
        #time.sleep(0.1)

        # handle LEDs
        for x in range(0, 8):
            for y in range(0, 8):
                clr = 1
                #if y > 2: clr = 1
                if y > 4: clr = 3
                if y > 5: clr = 2
                if y > vals[x]: clr = 0
                #clr = int(random.random() * 4)
                grid.setBicolorPixel(x, y, color=clr)

        if vals[8] < 4:
        	GPIO.output(LEDpin, True)
        else:
        	GPIO.output(LEDpin, False)
		
		if (GPIO.input(buttonPin) == True ):
			print 'restart backpack'
			grid = EightByEight(address=0x70)
			led = LEDBackpack(0x70)
			led.setBrightness(5)
			led.setBlinkRate(0)
			GPIO.output(LEDpin, True)
			time.sleep(1)
			GPIO.output(LEDpin, False)

    while False:
        # read the analog pin
        point = 1023 - readadc(adc_pin, SPICLK, SPIMOSI, SPIMISO, SPICS)
        if len(points) < 500:
            points.append(point)
        else:
            points[pointer] = point
        if pointer > 0:
        	# 1024 (max adc) / 320 (max y) = 3.2
            pts = (pointer - 1, points[pointer - 1] / 3.2, pointer, points[pointer] / 3.2)
            #myscope.draw_line(pts)
            time.sleep(0.01)
        pointer += 1
        if pointer >= 500:
            pointer = 0
            #myscope.drawGraticule()
        if DEBUG: print point
