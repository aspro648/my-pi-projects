import os
import pygame
import time
import random

class pyscope :
    screen = None
    
    def __init__(self):
        "Ininitializes a new pygame screen using the framebuffer"
        # Based on "Python GUI in Linux frame buffer"
        # http://www.karoltomala.com/blog/?p=679
        self.left_x = 75
        self.size_x = 500
        self.top_y = 30
        self.size_y = 320
        disp_no = os.getenv("DISPLAY")
        if disp_no:
            print "I'm running under X display = {0}".format(disp_no)
        
        # Check which frame buffer drivers are available
        # Start with fbcon since directfb hangs with composite output
        drivers = ['fbcon', 'directfb', 'svgalib']
        found = False
        for driver in drivers:
            # Make sure that SDL_VIDEODRIVER is set
            if not os.getenv('SDL_VIDEODRIVER'):
                os.putenv('SDL_VIDEODRIVER', driver)
            try:
                pygame.display.init()
            except pygame.error:
                print 'Driver: {0} failed.'.format(driver)
                continue
            found = True
            break
    
        if not found:
            raise Exception('No suitable video driver found!')
        
        size = (pygame.display.Info().current_w, pygame.display.Info().current_h)
        print "Framebuffer size: %d x %d" % (size[0], size[1])
        self.screen = pygame.display.set_mode(size, pygame.FULLSCREEN)
        # Clear the screen to start
        self.screen.fill((0, 0, 0))        
        # Initialise font support
        pygame.font.init()
        # Render the screen
        pygame.display.update()


    def __del__(self):
        "Destructor to make sure pygame shuts down, etc."

    
    def drawGraticule(self):
        "Renders an empty graticule"
        # The graticule is divided into 10 columns x 8 rows
        # Each cell is 50x40 pixels large, with 5 subdivisions per
        # cell, meaning 10x8 pixels each. Subdivision lines are
        # displayed on the central X and Y axis
        # Active area = 10,30 to 510,350 (500x320 pixels)
        borderColor = (255, 255, 255)
        bgColor = (0, 0, 0)
        lineColor = (64, 64, 64)
        subDividerColor = (128, 128, 128)
        # Outer border: 2 pixels wide
        #pygame.draw.rect(self.screen, borderColor, (8,28,504,324), 2)
        pygame.draw.rect(self.screen, bgColor, (self.left_x, self.top_y, self.size_x, self.size_y), 0)
        pygame.draw.rect(self.screen, borderColor, (self.left_x - 2, self.top_y - 2, self.size_x + 2, self.size_y + 2), 2)        
        # Horizontal lines (40 pixels apart)
        for i in range(1, 8):
            y = self.top_y + i * 40
            pygame.draw.line(self.screen, lineColor, (self.left_x, y), (self.size_x + self.left_x, y))
        # Vertical lines (50 pixels apart)
        for i in range(1, 10):
            x = self.left_x + i * 50
            pygame.draw.line(self.screen, lineColor, (x, self.top_y), (x, self.size_y + self.top_y))
        # Vertical sub-divisions (8 pixels apart)
        for i in range(1, 40):
            y = self.top_y + i * 8
            center_x = self.size_x / 2 + self.left_x
            pygame.draw.line(self.screen, subDividerColor, (center_x - 2, y), (center_x + 2, y))
        # Horizontal sub-divisions (10 pixels apart)
        for i in range(1, 50):
            x = self.left_x + i * 10
            center_y = self.size_y / 2 + self.top_y
            pygame.draw.line(self.screen, subDividerColor, (x, center_y - 2), (x, center_y + 2))   


    def draw_screen(self):
        "Draw inital screen with logo and text."
        adcColor = (255, 255, 0)  # Yellow
        self.drawGraticule()
        # Render the Adafruit logo at 10,360
        logo = pygame.image.load('adafruit_logo.gif').convert()
        self.screen.blit(logo, (self.left_x, self.size_y + self.top_y + 10))
        # Get a font and use it render some text on a Surface.
        font = pygame.font.Font(None, 30)
        text_surface = font.render('pyScope (%s)' % "0.1", 
            True, (255, 255, 255))  # White text
        # Blit the text at 10, 0
        self.screen.blit(text_surface, (self.left_x, 0))
        # Render some text with a background color
        text_surface = font.render('Channel 0',
            True, (0, 0, 0), (255, 255, 0)) # Black text with yellow BG
        # Blit the text
        self.screen.blit(text_surface, (475, 0))
        # Update the display
        pygame.display.update()

        
    def draw_line(self, points):
    	adcColor = (255, 255, 0)  # Yellow
    	x1, y1, x2, y2 = points
    	pygame.draw.line(self.screen, adcColor, (x1 + self.left_x, y1 + self.top_y), (x2 + self.left_x, y2 + self.top_y))
    	pygame.display.update()


    def draw_test(self):
            # Random adc data
            adcColor = (255, 255, 0)  # Yellow
            yLast = 260
            for x in range(0, self.size_x, 5):
                y = random.randrange(0, self.size_y, 2) 
                pts = (x, yLast, x + 1, y)
                print pts
                self.draw_line(pts)
                time.sleep(0.1)
                yLast = y


if __name__ == '__main__':
    # Create an instance of the PyScope class
    myscope = pyscope()
    myscope.draw_screen()
    myscope.draw_test()
    #time.sleep(1)
    #myscope.drawGraticule() # clear screen
    #myscope.draw_test()
    
    # Wait 10 seconds and exit
    time.sleep(10)
