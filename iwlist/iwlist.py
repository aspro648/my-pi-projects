#!/usr/bin/env python

import os, sys
from subprocess import Popen, PIPE
DEBUG = False

def getiwlist():
    process = Popen(['iwlist', 'wlan0', 'scan'], stdout=PIPE)
    output = process.stdout
    
    lines = output.readlines()
    if DEBUG: print len(lines)
    
    nodes = {}
    encryption = ''
    
    cell = 0
    
    for num, line in enumerate(lines):
        if DEBUG: print num, line
        if line != '\n':
            if 'Cell' == line.split()[0]:
                cell += 1
                nodes[cell] = {}
            if 'ESSID:' in line:
                essid = line.split(':')[1]
                essid = essid.strip()
                nodes[cell]['ESSID'] = essid.strip('"')
            if 'Encryption key:' in line:
                encryption = line.split(':')[1]
                nodes[cell]['encrypt'] = encryption.strip()
            if 'Quality=' in line.split()[0]:
                nodes[cell]['quality'] = line.split()[0].split('=')[1].split('/')[0] 
                nodes[cell]['level'] = line.split()[2].split('=')[1].split('/')[0] 
    
    if DEBUG: print nodes
    
    #delete encrypted 
    unencrypted = {}
    for node in nodes:
        if nodes[node]['encrypt'] == 'on':
            unencrypted[node] = nodes[node]
    
    # sort by level
    levels = []
    nodes = []
    for node in unencrypted:
        level = int(unencrypted[node]['level'])
        #print 'level = ', level
        if len(levels) == 0:
            levels.append(level)
            nodes.append(node)
        else:
            new_levels = []
            new_nodes = []
            found = False
            for num, item in enumerate(levels):
                #print found, level, item
                if not found:
                    if level <= item:
                        new_levels.append(item)
                        new_nodes.append(nodes[num])
                    else:
                        new_levels.append(level)
                        new_nodes.append(node)
                        new_levels.append(item)
                        new_nodes.append(nodes[num])
                        found = True
                else:
                    new_levels.append(item)
                    new_nodes.append(nodes[num])
            if not found:
                new_levels.append(level)
                new_nodes.append(node)
            levels = new_levels
            nodes = new_nodes
    return nodes, unencrypted

if __name__ == '__main__':
    nodes, unencrypted = getiwlist()
    #print 'nodes:'
    #print nodes
    #print levels
    for node in nodes:
        if unencrypted[node]['ESSID']:
            print node, unencrypted[node]['ESSID'], unencrypted[node]['level']