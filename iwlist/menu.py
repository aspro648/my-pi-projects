#!/usr/bin/python

from time import sleep
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate
import socket
import fcntl
import struct
import os, sys

# Initialize the LCD plate.  Should auto-detect correct I2C bus.  If not,
# pass '0' for early 256 MB Model B boards or '1' for all later versions
try:
    lcd = Adafruit_CharLCDPlate()
except IOError:
    print "No LCD?"
    sys.exit()
# Clear display and show greeting, pause 1 sec
lcd.clear()
lcd.message("Adafruit RGB LCD\nPlate w/Keypad!")
sleep(1)

# Cycle through backlight colors
colors = (lcd.RED , lcd.YELLOW, lcd.GREEN, lcd.TEAL,
       lcd.BLUE, lcd.VIOLET, lcd.ON, lcd.OFF)
colorNames = ('red', 'yellow', 'green', 'teal', 'blue', 'violet', 'on', 'off')

for color in range(len(colors)):
    lcd.backlight(colors[color])
    lcd.clear()
    lcd.message(colorNames[color])
    sleep(0.5)


def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])

nws = ['eth0', 'wlan0']
ips = []
host = socket.gethostname()

for nw in nws:
        try:
            ip = get_ip_address(nw)
            ips.append('%s-%s\n%s' % (host, nw, ip))
        except IOError: 
            ips.append('%s-%s\n%s' % (host, nw, 'none'))
            print "NO ", nw
ips.append('shutdown')
ips.append('reboot')
ips.append('color')
ips.append('exit')

color = 0
lcd.backlight(colors[color])

print ips
pos = 0
lcd.clear()
lcd.message(ips[pos])
  

while True:
    if lcd.buttonPressed(lcd.UP):
        pos += 1
        if pos >= len(ips):
            pos = 0
        lcd.clear()
        lcd.message(ips[pos])
        print pos
        sleep(0.5)
    if lcd.buttonPressed(lcd.DOWN):
        pos -= 1
        if pos < 0:
            pos = len(ips) - 1
        lcd.clear()
        lcd.message(ips[pos])
        print pos
        sleep(0.5)
    if lcd.buttonPressed(lcd.SELECT):
        #lcd.clear()
        sleep(0.5)
        if ips[pos] == 'exit':
            lcd.clear()
            lcd.message('exiting')
            print 'goodbye'
            sys.exit()
        if ips[pos] == 'reboot':
            lcd.clear()
            lcd.message('rebooting')
            print 'rebooting'
            os.system('sudo reboot')
        if ips[pos] == 'shutdown':
            lcd.clear()
            lcd.message('shuting down')
            print 'shutdown'
            os.system('sudo shutdown now')
        if ips[pos] == 'color':
                lcd.clear()
                lcd.message(colorNames[color])
                sleep(0.5)
                while True:
                    if lcd.buttonPressed(lcd.SELECT):
                        lcd.clear()
                        pos = 0
                        lcd.message(ips[pos])
                        sleep(0.25)
                        break
                    if lcd.buttonPressed(lcd.UP):
                        sleep(0.25)
                        color += 1
                        if color >= len(colors):
                            color =0
                        lcd.clear()
                        lcd.message(colorNames[color])
                        lcd.backlight(colors[color])
                    if lcd.buttonPressed(lcd.DOWN):
                        color -= 1
                        sleep(0.25)
                        if color < 0:
                            color = len(colors) - 1
                        lcd.clear()
                        lcd.message(colorNames[color])
                        lcd.backlight(colors[color])
 
