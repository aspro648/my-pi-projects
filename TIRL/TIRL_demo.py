import TIRL as turtle  # To use TIRL robot library
#import turtle as turtle # to use built-in python graphics
import TurtleFont

t = turtle.Turtle()     # Create object that has Turtle commands
f = TurtleFont.Font(t)  # Create object that has vector fonts


# Write your name with TurtleFont
f.K(scale=30) # a little bigger than the rest
f.E()
f.N()

# Move position to prior to drawing.
t.left(90)
t.forward(60)
t.right(90)
t.pendown()

# Draw a star with Turtle commands!
for x in range(2):
    t.forward(100)  # Distance in mm (about 4 inches)
    t.left(36)      # Degrees
    t.backward(100)
    t.left(36)
t.forward(100)

turtle.done() # exit gracefully