# Implenting Turtle graphics for a low-cost, 3D-printed,
# robotic platform called TIRL (Turtle In Real Life).
# 
# Ken Olsen, June 2015
# http://MakersBox.blogspot.com
# 648.ken@gmail.com

# The following tutorials helped:
#   https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/
#   https://learn.adafruit.com/adafruits-raspberry-pi-lesson-10-stepper-motors
#   https://learn.adafruit.com/mcp230xx-gpio-expander-on-the-raspberry-pi
#   https://learn.adafruit.com/adafruits-raspberry-pi-lesson-5-using-a-console-cable

# Turtle resources on-line
#   https://blockly-games.appspot.com/turtle (Scratch like blocks)
#   http://www.skulpt.org (Python, should be cut-n-paste with TIRL)

import sys, time
import RPi.GPIO as GPIO
from Adafruit_MCP230xx import Adafruit_MCP230XX
import TurtleFont


PEN_UP = 90
PEN_DOWN = 10 #15

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)
pwm = GPIO.PWM(18, PEN_UP)
pwm.start(PEN_UP)


class Turtle():
    ''' Implent Turtle commands for TIRL Robot
    Turtle graphics is a popular way for introducing programming to
    kids. It was part of the original Logo programming language developed
    by Wally Feurzig and Seymour Papert in 1966.
    
    Imagine a robotic turtle starting at (0, 0) in the x-y plane.
    After an ``import turtle``, give it
    the command turtle.forward(100), and it moves 100 millimeters in
    the direction it is facing, drawing a line as it moves. Give it the
    command turtle.right(45), and it rotates in-place 45 degrees clockwise.
    
    By combining together these and similar commands, intricate shapes and
    pictures can easily be drawn.

    Units:
        Distance = millimeters
        Angle = degrees
    
    Optional keyword args:
        scale=2.5   # Scale for letters, 2.5x = 5cm x 10cm 
        debug=False # Turns on extra consol output
        wheel_dia=77,      # mm (was 68mm for original wheel)
        wheel_base=122,    # mm  
        steps_rev=128,     # 512 for 64x gearbox, 128 for 16x gearbox
        delay=2            # time between steps in ms

    Example:
        import TIRL as turtle
        
        t = turtle.Turtle()
        
        for x in range(2):  # Draw a star!
            t.forward(100)  # in millimeters, or about 4 inches
            t.left(36)      # Degrees
            t.backward(100)
            t.left(36)
        t.forward(100)

        turtle.done()
    '''
    
    scale = 25

    def __init__(self, scale=25, debug=False,
                 wheel_dia=77,      # mm (was 68mm for original wheel)
                 wheel_base=128,    # mm  
                 steps_rev=122.8,     # 512 for 64x gearbox, 128 for 16x gearbox
                 delay=2            # time between steps in ms
                 ):
        Turtle.scale = scale # for letters, 25x = 2cm x 10cm
        self.debug = debug
        self.wheel_dia = wheel_dia  
        self.wheel_base = wheel_base 
        self.steps_rev = steps_rev     
        self.delay = delay 

        # Stepper sequence org->pink->blue->yel
        self.L_stepper_pins = [0, 2, 3, 1]
        self.R_stepper_pins = [4, 6, 7, 5]

        self.fwd_mask =  [[1, 0, 1, 0],
                         [0, 1, 1, 0],
                         [0, 1, 0, 1],
                         [1, 0, 0, 1]]

        self.rev_mask  = self.fwd_mask[::-1]

        self.L_squence_pos = 0
        self.R_squence_pos = 0

        # Set MCP pins to OUTPUT and low
        self.mcp = Adafruit_MCP230XX(address = 0x20, num_gpios = 8) # MCP23008 
        for pin in self.L_stepper_pins:
            self.mcp.config(pin, self.mcp.OUTPUT)
            self.mcp.output(pin, 0)

        for pin in self.R_stepper_pins:
            self.mcp.config(pin, self.mcp.OUTPUT)
            self.mcp.output(pin, 0)
            
        # jog to get the servos engaged
        self.penup()
        for i in range(0, 4):
            for m in range(len(self.fwd_mask)):
                self.__setStep(self.L_stepper_pins, self.rev_mask[m])
                self.__setStep(self.R_stepper_pins, self.fwd_mask[m])
                time.sleep(self.delay / 1000)        
                   

    def step(self, distance):
        '''Calulates the number of steps based on
            distance in centimeters.'''
        steps = distance * self.steps_rev / (self.wheel_dia * 3.1412)
        return steps


    def forward(self, distance):
        '''
        Move the turtle forward by the specified distance in millimeters.
        
        Aliases: forward | fd
        
        Argument:
        distance -- a number (integer or float)
        
        Move the turtle forward by the specified distance, in the direction
        the turtle is headed.
        
        Example:
        >>> position()
        (0.00, 0.00)
        >>> forward(25)
        >>> position()
        (25.00,0.00)
        >>> forward(-75)
        >>> position()
        (-50.00,0.00)
        '''
        steps = self.step(distance)
        if self.debug:
            print("forward(%s mm), %s steps" % (distance, steps))
        for i in range(0, int(steps)):
            for m in range(len(self.fwd_mask)):
                self.__setStep(self.L_stepper_pins, self.rev_mask[m])
                self.__setStep(self.R_stepper_pins, self.fwd_mask[m])
                time.sleep(self.delay / 1000)

    def fd(self, distance):
        '''Alias for forward().'''
        self.forward(distance)
        

    def backward(self, distance):
        '''
        Move the turtle backward by distance (in millimeters).
        
        Aliases: back | backward | bk
        
        Argument:
        distance -- a number
        
        Move the turtle backward by distance ,opposite to the direction the
        turtle is headed. Do not change the turtle's heading.
        
        Example:
        >>> position()
        (0.00, 0.00)
        >>> backward(30)
        >>> position()
        (-30.00, 0.00)
        '''
        steps = self.step(distance)    
        if self.debug:
            print("backward(%s cm), %s steps" % (distance, steps))
        for i in range(0, int(steps)):
            for m in range(len(self.fwd_mask)):
                self.__setStep(self.L_stepper_pins, self.fwd_mask[m])
                self.__setStep(self.R_stepper_pins, self.rev_mask[m])
                time.sleep(self.delay / 1000)

    def back(self, distance):
        ''' Alias for backward().'''
        self.backward(distance)

    def bk(self, distance):
        ''' Alias for backward().'''
        self.backward(distance)        


    def __setStep(self, pins, mask):
        '''Send mask patern to mcp'''
        for i, pin in enumerate(pins):
            self.mcp.output(pin, mask[i])


    def right(self, degrees):
        '''
        Turn turtle right by angle units.
        
        Aliases: right | rt
        
        Argument:
        angle -- a number (integer or float)
        
        Turn turtle right by angle units. (Units are by default degrees,
        but can be set via the degrees() and radians() functions.)
        Angle orientation depends on mode. (See this.)
        
        Example:
        >>> heading()
        22.0
        >>> right(45)
        >>> heading()
        337.0
        '''       
        rotation = degrees / 360.0
        distance = self.wheel_base * 3.1412 * rotation
        steps = self.step(distance)
        if self.debug:
            print("right(%s deg)=%s steps" % (degrees, steps))
        for i in range(0, int(steps)):
            for m in range(len(self.fwd_mask)):
                self.__setStep(self.L_stepper_pins, self.rev_mask[m])
                self.__setStep(self.R_stepper_pins, self.rev_mask[m])
                time.sleep(self.delay / 1000)    

    def rt(self, degrees):
        '''Alias for right().'''
        self.right(degrees)
        

    def left(self, degrees):
        '''
        Turn turtle left by angle units.
        
        Aliases: left | lt
        
        Argument:
        angle -- a number (integer or float)
        
        Turn turtle left by angle units. (Units are by default degrees,
        but can be set via the degrees() and radians() functions.)
        Angle orientation depends on mode. (See this.)
        
        Example:
        >>> heading()
        22.0
        >>> left(45)
        >>> heading()
        67.0
        '''
        rotation = degrees / 360.0
        distance = self.wheel_base * 3.1412 * rotation
        steps = self.step(distance)
        if self.debug:
            print("left(%s deg)=%s steps" % (degrees, steps))
        for i in range(0, int(steps)):
            for m in range(len(self.fwd_mask)):
                self.__setStep(self.L_stepper_pins, self.fwd_mask[m])
                self.__setStep(self.R_stepper_pins, self.fwd_mask[m])
                time.sleep(self.delay / 1000)

    def lt(self, angle):
        '''Alias for left().'''
        self.left(angle)


    def penup(self):
        '''
        Pull the pen up -- no drawing when moving.
        
        Aliases: penup | pu | up
        
        No argument
        
        Example:
        >>> penup()
        '''
        if self.debug:
            print("penup()")

        pwm.ChangeDutyCycle(PEN_UP)
        return

    def pu(self):
        '''Alias for penup().'''
        self.penup()
        
    def up(self):
        '''Alias for penup().'''
        self.penup()
        

    def pendown(self):
        '''
        Pull the pen down -- drawing when moving.
        
        Aliases: pendown | pd | down
        
        No argument.
        
        Example:
        >>> pendown()
        '''
        if self.debug:
            print("pendown()")
        pwm.ChangeDutyCycle(PEN_DOWN)
        return

    def pd(self):
        '''Alias for pendown().'''
        self.pendown()

    def down(self):
        '''Alias for pendown().'''
        self.pendown()


    def hyp(self, a, b):
        ''' Return hypotoneus using Pythagorean Theory '''
        return math.sqrt(a**2 + b**2)   


if __name__ == '__main__':

    import TIRL as turtle

    t = turtle.Turtle(debug=True)
    
    t.pendown()
    for x in range(2):  # Draw a star!
        t.forward(100)  # in millimeters, or about 4 inches
        t.left(36)      # Degrees
        t.backward(100)
        t.left(36)
    t.forward(100)

    turtle.done()


def done():
    '''
    Exit tk() gracefully in Turtle. For TIRL this isn't needed 
    pass
    '''


