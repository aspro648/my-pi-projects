
#import TIRL as turtle  # To use TIRL robot library
import TIRL as turtle # to use built-in python graphics
import TurtleFont

t = turtle.Turtle()     # Create object that has Turtle commands
f = TurtleFont.Font(t)  # Create object that has vector fonts



# Draw a star with Turtle commands!
side_length = 5

t.pendown()
for x in range(100):
    t.forward(side_length)  # Distance in mm (about 4 inches)
    t.left(90)      # Degrees
    side_length = side_length + 2


turtle.done() # exit gracefully