import math


def hyp(a, b):
    ''' Return hypotoneus using Pythagorean Theory '''
    return math.sqrt(a**2 + b**2)  



class Font:
    ''' A set of vector fonts for Turtle graphics based
    on a 2 x 4 grid. All letters assume turtle starts in
    lower left corner facing East(default Turtle position),
    although with the pen up (TIRL bleeds ink otherwise).
    A scale of 25 give a 100 pixel high letter (or 10 cm
    with TIRL robot).

    Example:
        import turtle
        import TurtleFont

        t = turtle.Turtle()
        f = TurtleFont.Font(t)

        f.K(scale=30) # a little bigger than the rest
        f.E()
        f.N()
    '''
    
    scale = 15

    def __init__(self, turtle, scale=25):
        self.turtle = turtle
        Font.scale = scale
        print(Font.scale)
    
    def A(self, scale=scale):
        self.turtle.left(90)
        self.turtle.pendown()
        self.turtle.forward(3 * scale)
        self.turtle.right(45)
        self.turtle.forward(1.41 * scale)
        self.turtle.right(90)
        self.turtle.forward(1.41 * scale)
        self.turtle.right(45)
        self.turtle.forward(2 * scale)
        self.turtle.right(90)
        self.turtle.forward(2 * scale)
        self.turtle.backward(2 * scale)
        self.turtle.left(90)
        self.turtle.forward(1 * scale)
        self.turtle.penup()
        self.turtle.left(90)
        self.turtle.forward(1 * scale)

    def B(self, scale=scale):
        self.turtle.pendown()
        self.turtle.forward(1 * scale)
        self.turtle.left(45)
        self.turtle.forward(1.41 * scale)
        self.turtle.left(90)
        self.turtle.forward(1.41 * scale)
        self.turtle.right(90)
        self.turtle.forward(1.41 * scale)
        self.turtle.left(90)
        self.turtle.forward(1.41 * scale)
        self.turtle.left(45)
        self.turtle.forward(1 * scale)
        self.turtle.left(90)
        self.turtle.forward(4 * scale)
        self.turtle.penup()
        self.turtle.left(90)
        self.turtle.forward(3 * scale)    

    def C(self, scale=scale):
        self.turtle.left(90 - 33.7)
        self.turtle.forward(hyp(2, 3) * scale)
        self.turtle.pendown()
        self.turtle.left(78.7)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(90)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(45)
        self.turtle.forward(2 * scale)
        self.turtle.left(45)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(90)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.penup()
        self.turtle.right(90)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(45)

    def D(self, scale=scale):
        self.turtle.pendown()
        self.turtle.forward(1 * scale)
        self.turtle.left(45)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(45)
        self.turtle.forward(2 * scale)
        self.turtle.left(45)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(45)
        self.turtle.forward(1 * scale)
        self.turtle.left(90)
        self.turtle.forward(4 * scale)
        self.turtle.penup()
        self.turtle.left(90)
        self.turtle.forward(3 * scale)

    def E(self, scale=scale):
        self.turtle.left(90)
        self.turtle.forward(4 * scale)
        self.turtle.right(90)
        self.turtle.forward(2 * scale)
        self.turtle.pendown()
        self.turtle.backward(2 * scale)
        self.turtle.right(90)
        self.turtle.forward(2 * scale)
        self.turtle.left(90)
        self.turtle.forward(1 * scale)
        self.turtle.backward(1 * scale)
        self.turtle.right(90)
        self.turtle.forward(2 * scale)
        self.turtle.left(90)
        self.turtle.forward(2 * scale)
        self.turtle.penup()
        self.turtle.forward(1 * scale)

    def F(self, scale=scale):
        self.turtle.left(90)
        self.turtle.pendown()
        self.turtle.forward(4 * scale)
        self.turtle.right(90)
        self.turtle.forward(2 * scale)
        self.turtle.penup()
        self.turtle.left(90 - 26.6)
        self.turtle.backward(hyp(2, 1) * scale)
        self.turtle.right(90 - 26.6)
        self.turtle.backward(1 * scale)
        self.turtle.pendown()
        self.turtle.forward(1 * scale)
        self.turtle.penup()
        self.turtle.right(45)
        self.turtle.forward(hyp(2, 2) * scale)
        self.turtle.left(45)

    def G(self, scale=scale):
        self.turtle.left(90 - 33.7)
        self.turtle.forward(hyp(2, 3) * scale)
        self.turtle.pendown()
        self.turtle.left(78.7)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(90)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(45)
        self.turtle.forward(2 * scale)
        self.turtle.left(45)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(90)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(45)
        self.turtle.forward(1 * scale)
        self.turtle.right(90)
        self.turtle.backward(1 * scale)
        self.turtle.penup()
        self.turtle.right(45)
        self.turtle.forward(hyp(2, 2) * scale)
        self.turtle.left(45)

    def H(self, scale=scale):
        self.turtle.left(90)
        self.turtle.pendown()
        self.turtle.forward(4 * scale)
        self.turtle.backward(2 * scale)
        self.turtle.right(90)
        self.turtle.forward(2 * scale)
        self.turtle.left(90)
        self.turtle.forward(2 * scale)
        self.turtle.backward(4 * scale)
        self.turtle.penup()
        self.turtle.right(90)
        self.turtle.forward(1 * scale)

    def I(self, scale=scale):
        self.turtle.left(90 - 14)
        self.turtle.forward(hyp(4, 1) * scale)
        self.turtle.left(14)
        self.turtle.pendown()
        self.turtle.backward(4 * scale)
        self.turtle.penup()
        self.turtle.right(90)
        self.turtle.forward(2 * scale)

    def J(self, scale=scale):
        self.turtle.left(90)
        self.turtle.forward(1 * scale)
        self.turtle.pendown()
        self.turtle.backward(1 * scale)
        self.turtle.left(90)
        self.turtle.backward(2 * scale)
        self.turtle.left(90)
        self.turtle.backward(4 * scale)
        self.turtle.penup()
        self.turtle.left(14)
        self.turtle.forward(hyp(1, 4) * scale)
        self.turtle.left(90 - 14)
     
    def K(self, scale=scale):
        self.turtle.left(90)
        self.turtle.pendown()
        self.turtle.forward(4 * scale)
        self.turtle.backward(2 * scale)
        self.turtle.right(45)
        self.turtle.forward(2.83 * scale)
        self.turtle.backward(2.83 * scale)
        self.turtle.right(90)
        self.turtle.forward(2.83 * scale)
        self.turtle.penup()
        self.turtle.left(45)
        self.turtle.forward(1 * scale)

    def L(self, scale=scale):
        self.turtle.left(90)
        self.turtle.forward(4 * scale)
        self.turtle.pendown()
        self.turtle.backward(4 * scale)
        self.turtle.right(90)
        self.turtle.forward(2 * scale)
        self.turtle.penup()
        self.turtle.forward(1 * scale)

    def M(self, scale=scale):
        self.turtle.left(90)
        self.turtle.pendown()
        self.turtle.forward(4 * scale)
        self.turtle.left(26.6)
        self.turtle.backward(hyp(2, 1) * scale)
        self.turtle.right(26.6 + 26.6)
        self.turtle.forward(hyp(2, 1) * scale)
        self.turtle.left(26.6)
        self.turtle.backward(4 * scale)
        self.turtle.penup()
        self.turtle.right(90)
        self.turtle.forward(1 * scale)

    def N(self, scale=scale):
        self.turtle.left(90)
        self.turtle.pendown()
        self.turtle.forward(4 * scale)
        self.turtle.left(26.6)
        self.turtle.backward(4.47 * scale)
        self.turtle.right(26.6)
        self.turtle.forward(4 * scale)
        self.turtle.penup()
        self.turtle.backward(4 * scale)
        self.turtle.right(90)
        self.turtle.forward(1 * scale)

    def O(self, scale=scale):
        self.turtle.forward(1 * scale)
        self.turtle.left(45)
        self.turtle.pendown()
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(45)
        self.turtle.forward(2 * scale)
        self.turtle.left(45)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(90)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(45)
        self.turtle.forward(2 * scale)
        self.turtle.left(45)
        self.turtle.forward(hyp(1, 1) * scale)  
        self.turtle.penup()
        self.turtle.left(45)
        self.turtle.forward(2 * scale)

    def P(self, scale=scale):
        self.turtle.right(90)
        self.turtle.pendown()
        self.turtle.backward(4 * scale)
        self.turtle.right(90)
        self.turtle.backward(1 * scale)
        self.turtle.right(45)
        self.turtle.backward(hyp(1, 1) * scale)
        self.turtle.right(90)
        self.turtle.backward(hyp(1, 1) * scale)
        self.turtle.right(45)
        self.turtle.backward(1 * scale)
        self.turtle.penup()
        self.turtle.right(33.7)
        self.turtle.forward(hyp(3, 2) * scale)
        self.turtle.left(33.7)

    def Q(self, scale=scale):
        self.turtle.forward(1 * scale)
        self.turtle.left(45)
        self.turtle.pendown()
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(45)
        self.turtle.forward(2 * scale)
        self.turtle.left(45)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(90)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(45)
        self.turtle.forward(2 * scale)
        self.turtle.left(45)
        self.turtle.forward(hyp(1, 1) * scale)  
        self.turtle.penup()
        self.turtle.left(45)
        self.turtle.forward(1 * scale)
        self.turtle.right(45)
        self.turtle.pendown()
        self.turtle.backward(hyp(1, 1) * scale)
        self.turtle.penup()
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(45)
        self.turtle.forward(1 * scale)

    def R(self, scale=scale):
        self.turtle.right(90)
        self.turtle.pendown()
        self.turtle.backward(4 * scale)
        self.turtle.right(90)
        self.turtle.backward(1 * scale)
        self.turtle.right(45)
        self.turtle.backward(hyp(1, 1) * scale)
        self.turtle.right(90)
        self.turtle.backward(hyp(1, 1) * scale)
        self.turtle.right(45)
        self.turtle.backward(1 * scale)
        self.turtle.right(45)
        self.turtle.forward(hyp(2, 2) * scale)
        self.turtle.penup()
        self.turtle.left(45)
        self.turtle.forward(1 * scale)

    def S(self, scale=scale):
        self.turtle.pendown()
        self.turtle.forward(1 * scale)
        self.turtle.left(45)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.left(90)
        self.turtle.forward(hyp(2, 2) * scale)
        self.turtle.right(90)
        self.turtle.forward(hyp(1, 1) * scale)
        self.turtle.right(45)
        self.turtle.forward(1 * scale)
        self.turtle.penup()
        self.turtle.right(76)
        self.turtle.forward(hyp(4, 1) * scale)
        self.turtle.left(76)
        
    def T(self, scale=scale):
        self.turtle.forward(1 * scale)
        self.turtle.left(90)
        self.turtle.pendown()
        self.turtle.forward(4 * scale)
        self.turtle.right(90)
        self.turtle.backward(1 * scale)
        self.turtle.forward(2 * scale)
        self.turtle.penup()
        self.turtle.right(76)
        self.turtle.forward(hyp(4, 1) * scale)
        self.turtle.left(76)

    def U(self, scale=scale):
        self.turtle.left(90)
        self.turtle.forward(4 * scale)
        self.turtle.pendown()
        self.turtle.backward(4 * scale)
        self.turtle.left(90)
        self.turtle.backward(2 * scale)
        self.turtle.left(90)
        self.turtle.backward(4 * scale)
        self.turtle.penup()
        self.turtle.left(14)
        self.turtle.forward(hyp(4, 1) * scale)
        self.turtle.left(76)

    def V(self, scale=scale):
        self.turtle.left(90)
        self.turtle.forward(4 * scale)
        self.turtle.pendown()
        self.turtle.backward(3 * scale)
        self.turtle.left(45)
        self.turtle.backward(hyp(1, 1) * scale)
        self.turtle.left(90)
        self.turtle.backward(hyp(1, 1) * scale)
        self.turtle.left(45)
        self.turtle.backward(3 * scale)
        self.turtle.penup()
        self.turtle.left(14)
        self.turtle.forward(hyp(4, 1) * scale)
        self.turtle.left(76)

    def W(self, scale=scale):
        self.turtle.right(90)
        self.turtle.backward(4 * scale)
        self.turtle.pendown()
        self.turtle.forward(hyp(4, 1) * scale)
        self.turtle.right(26.6)
        self.turtle.backward(hyp(2, 1) * scale)
        self.turtle.left(26.6 + 26.6)
        self.turtle.forward(hyp(2, 1) * scale)
        self.turtle.right(26.6)
        self.turtle.backward(4 * scale)
        self.turtle.penup()
        self.turtle.left(14)
        self.turtle.forward(hyp(4, 1) * scale)
        self.turtle.left(76)

    def X(self, scale=scale):
        self.turtle.left(90 - 26.6)
        self.turtle.pendown()
        self.turtle.forward(hyp(4, 2) * scale)
        self.turtle.penup()
        self.turtle.right(90 - 26.6)
        self.turtle.back(2 * scale)
        self.turtle.right(90 - 26.6)
        self.turtle.pendown()
        self.turtle.forward(hyp(4, 2) * scale)
        self.turtle.penup()
        self.turtle.left(90 - 26.6)
        self.turtle.forward(1 * scale)

    def Y(self, scale=scale):
        self.turtle.right(90)
        self.turtle.backward(4 * scale)
        self.turtle.left(27)
        self.turtle.pendown()
        self.turtle.forward(hyp(2, 1) * scale)
        self.turtle.right(27 + 27)
        self.turtle.backward(hyp(2, 1) * scale)
        self.turtle.forward(hyp(2, 1) * scale)
        self.turtle.left(27)
        self.turtle.forward(2 * scale)
        self.turtle.penup()
        self.turtle.left(90)
        self.turtle.forward(2 * scale)

    def Z(self, scale=scale):
        self.turtle.left(90)
        self.turtle.forward(4 * scale)
        self.turtle.right(90)
        self.turtle.pendown()
        self.turtle.forward(2 * scale)
        self.turtle.left(90 - 26.6)
        self.turtle.backward(hyp(4, 2) * scale)
        self.turtle.right(90 - 26.6)
        self.turtle.forward(2 * scale)
        self.turtle.penup()
        self.turtle.forward(1 * scale)

    def spc(self, scale=scale):
        ''' Create a space equal to one character. '''
        self.turtle.forward(3 * scale)


if __name__ == '__main__':

    import turtle
    import TurtleFont

    t = turtle.Turtle()
    f = TurtleFont.Font(t)

    f.K(scale=30) # a little bigger than the rest
    f.E()
    f.N()

    turtle.done()
