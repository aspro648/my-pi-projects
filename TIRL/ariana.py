import TIRL as turtle  # To use TIRL robot library
#import turtle as turtle # to use built-in python graphics
import TurtleFont
import time, sys


t = turtle.Turtle()     # Create object that has Turtle commands
f = TurtleFont.Font(t, scale=10)  # Create object that has vector fonts





# Move position to prior to drawing.
t.pendown()

f.A()
f.R()
f.I()
f.A()
f.N()
f.A()
t.penup()


turtle.done() # exit gracefully